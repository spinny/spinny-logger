require 'spinny/logger/version'
require 'letty'

module Spinny
  module Logger
    fn _log = ->(io, level, condition, text) do
      text = "{time} [{level}] {text}".format({
        level: level.ljust(5),
        time: Time.now.strftime("%D %H:%M:%S"),
        text: text,
      })

      condition =
        if condition.respond_to?(:call)
          condition.call
        else
          condition
        end

      io.puts(text) if condition

      text
    end

    fn log   = :_log.($stdout, 'INFO', true)
    fn info  = :log.()
    fn warn  = :_log.($stderr, 'WARN', true)
    fn debug = :_log.($stderr, 'DEBUG', ->{$DEBUG})
  end
end
